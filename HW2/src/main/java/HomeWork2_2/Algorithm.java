package HomeWork2_2;


public class Algorithm {

    public static void Menu(int algorithm,int n, int loopType)
    {
            switch(algorithm)
            {
                case 1: fibonacci(n, loopType);
                    break;
                case 2: factorial(n, loopType);
                    break;
                default: System.out.println("Введите 2-е значение [1-2]");
            }
    }
    public static int factorial(int n, int loopType){
        if(n < 0) throw new ArithmeticException("Число меньше нуля");
        int factorial = 1;
        int index = 1;
        if (loopType == 1) {
            for (; index <= n; index++) {
                factorial *= index;
            }
            System.out.println(factorial + " Цикл for");
        }
        else if (loopType == 2) {
            while (index <= n) {
                factorial *= index;
                index++;
            }
            System.out.println(factorial + " Цикл while");
        }
        else if(loopType == 3)
        {
            do{
                factorial *= index;
                index++;
            }
            while(index <= n);
            System.out.println(factorial + " Цикл do...while");
        }
        else {
            System.out.println("Введите 2-е значение [1-3]");
        }
        return factorial;
    }

    public static int[] fibonacci(int n,int loopType)
    {
        if(n <= 0) throw new ArithmeticException("Количество чисел не может быть меньше единицы");
        int[] arr = new int[n];
        arr[0] = 0;
        arr[1] = 1;
        int i = 2;
        if(loopType == 1){
            for (; i < n; ){
                arr[i] = arr[i - 1] + arr[i - 2];
                i++;
            }
            System.out.println("Цикл for");
        }
        else if(loopType == 2){
            while (i < n) {
                arr[i] = arr[i - 1] + arr[i - 2];
                i++;
            }
            System.out.println("Цикл while");
        }
        else if(loopType == 3)
        {
            do{
                arr[i] = arr[i - 1] + arr[i - 2];
                i++;
            }
            while(i < n);
            System.out.println("Цикл do...while");
        }
        else {
            System.out.println("Введите 2-е значение [1-3]");
        }
        PrintArr(arr);
        return arr;
    }
    public static void PrintArr(int[] arr){
        for(int index = 0;index < arr.length;index++)
        {
            System.out.print(arr[index] + " \n");

        }
    }
}
