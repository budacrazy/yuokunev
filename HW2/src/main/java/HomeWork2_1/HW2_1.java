package HomeWork2_1;

public class HW2_1 {
    public static void main(String[] args) {
        try{
            int a = Integer.parseInt(args[0]);
            int p = Integer.parseInt(args[1]);
            double m1 = Double.parseDouble(args[2]);
            double m2 = Double.parseDouble(args[3]);
            System.out.println("Значение равно = " + Calculate(a, p, m1, m2));
        }
        catch (ArithmeticException e){
            System.out.println("Значения введены некорректно");
        }

    }

    public static double Calculate(int a, int p, double m1, double m2) {
        if((Math.pow(p, 2) * (m1 + m2) == 0))
            throw new ArithmeticException();
        return  4 * Math.pow(Math.PI, 2) * Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2));
    }
}
