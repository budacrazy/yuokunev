package HW2_2;
import HomeWork2_2.Algorithm;
import org.junit.Assert;
import org.junit.Test;
public class FibonacciTest {
    int[] fib = new int[]{0, 1, 1, 2, 3, 5, 8, 13};
    int n = 8;

    @Test
    public void testCalculateWhileLoop(){
        Assert.assertArrayEquals(fib, Algorithm.fibonacci(n,1));
    }

    @Test
    public void testCalculateDoWhileLoop(){
        Assert.assertArrayEquals(fib, Algorithm.fibonacci(n, 2));
    }

    @Test
    public void testCalculateForLoop(){
        Assert.assertArrayEquals(fib, Algorithm.fibonacci(n, 3));
    }

    @Test
    public void testCalculateWhileLoopException(){
        Algorithm.fibonacci(-5, 1);
    }

    @Test
    public void testCalculateDoWhileLoopException(){
        Algorithm.fibonacci(0, 2);
    }

    @Test
    public void testCalculateForLoopException(){
        Algorithm.fibonacci(-1, 3);
    }
}
