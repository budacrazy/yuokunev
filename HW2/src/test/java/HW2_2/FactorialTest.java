package HW2_2;

import HomeWork2_2.Algorithm;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class FactorialTest {
    @Test
    public void testCalculateWhileLoopNoException(){assertEquals(120, Algorithm.factorial(5,1));}

    @Test
    public void testCalculateDoWhileLoopNoException(){
        assertEquals(1, Algorithm.factorial(0,2));
    }

    @Test
    public void testCalculateForLoopNoException(){
        assertEquals(24, Algorithm.factorial(4,3));
    }

    @Test
    public void testCalculateWhileLoopException(){Algorithm.factorial(-1,3);}

    @Test
    public void testCalculateDoWhileLoopException(){
        Algorithm.factorial(-1,2);
    }

    @Test
    public void testCalculateForLoopException(){Algorithm.factorial(-1,1);
    }
}
