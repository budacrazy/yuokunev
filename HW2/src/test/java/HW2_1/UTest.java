package HW2_1;
import HomeWork2_1.HW2_1;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class UTest {
    @Test
    public void testCalculateNoException(){assertEquals(3.89, HW2_1.Calculate(2, 3, 4,5), 0.01);}

    @Test
    public void testCalculateNegativeValues(){assertEquals(-3.89, HW2_1.Calculate(-2, -3, 4,5), 0.01); }
}
