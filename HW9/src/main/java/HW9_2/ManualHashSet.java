package HW9_2;

import java.util.HashSet;

public class ManualHashSet {

    public static HashSet Union(HashSet firstSet, HashSet secondSet) {

        HashSet resultSet = new HashSet();
        for (Object firstSetItem : firstSet) {
            resultSet.add(firstSetItem);
            for (Object secondSetItem : secondSet) {
                if (firstSetItem != secondSetItem) {
                    resultSet.add(secondSetItem);
                }
            }
        }
        return resultSet;
    }
    public static HashSet Intersection(HashSet firstSet, HashSet secondSet) {

        HashSet resultSet = new HashSet<>();
        for (Object firstSetItem : firstSet) {
            for (Object secondSetItem : secondSet) {
                if (firstSetItem == secondSetItem) {
                    resultSet.add(firstSetItem);
                }
            }
        }
        return resultSet;
    }
    public static HashSet Minus(HashSet firstSet, HashSet secondSet) {

        HashSet resultSet = new HashSet();
        for (Object firstSetItem : firstSet) {
            resultSet.add(firstSetItem);
            for (Object secondSetItem : secondSet) {
                if (firstSetItem == secondSetItem) {
                    resultSet.remove(firstSetItem);
                }
            }
        }
        return resultSet;
    }
    public static HashSet Difference(HashSet firstSet, HashSet secondSet) {

        HashSet resultSet = new HashSet();
        for (Object firstSetItem : firstSet) {
            if (!secondSet.contains(firstSetItem)) {
                resultSet.add(firstSetItem);
            }
        }

        for (Object secondSetItem : secondSet) {
            if (!firstSet.contains(secondSetItem)) {
                resultSet.add(secondSetItem);
            }
        }
        return resultSet;
    }
}
