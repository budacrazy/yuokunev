package HW9_1;

import java.util.Comparator;
import java.util.InputMismatchException;

public class ArrayComparator implements Comparator<Integer>{

    @Override
    public int compare(Integer firstNumber, Integer secondNumber) {
        int sumValue1 = 0;
        int sumValue2 = 0;
        try {
        for(int index = 0;index < String.valueOf(firstNumber).length(); index++){
            sumValue1 += Character.getNumericValue(String.valueOf(firstNumber).charAt(index));
        }
        for(int index = 0;index < String.valueOf(secondNumber).length(); index++){
            sumValue2 += Character.getNumericValue(String.valueOf(secondNumber).charAt(index));
        }
        }
        catch (NumberFormatException numberFormatException){
            System.out.println("Its Letter or Symbol");
        }
        return sumValue1 - sumValue2;
    }
}