package HW9_2;

import org.junit.Test;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ManualHashSetTest {

    private HashSet<Object> firstSet = new HashSet<>(List.of("A","B"));
    private HashSet<Object> secondSet = new HashSet<>(List.of("B","C"));

    @Test
    public void testSetUnion() {
        HashSet<Object> resultSet = new HashSet<>(List.of("A","B","C"));
        assertEquals(resultSet, ManualHashSet.Union(firstSet, secondSet));
    }
    @Test
    public void testSetIntersection() {
        HashSet<Object> resultSet = new HashSet<>(List.of("B"));
        assertEquals(resultSet, ManualHashSet.Intersection(firstSet, secondSet));
    }
    @Test
    public void testSetMinus() {
        HashSet<Object> resultSet = new HashSet<>(List.of("A"));
        assertEquals(resultSet, ManualHashSet.Minus(firstSet, secondSet));
    }
    @Test
    public void testSetDifference() {
        HashSet<Object> resultSet = new HashSet<>(List.of("A","C"));
        assertEquals(resultSet, ManualHashSet.Difference(firstSet, secondSet));
    }
}
