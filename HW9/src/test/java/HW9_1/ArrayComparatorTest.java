package HW9_1;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class ArrayComparatorTest {

    @Test
    public void testArraySorting() {
        Integer[] array = {6, 16, 8, 4, 83, 97, 82};
        Integer[] resultArray = {4, 6, 16, 8, 82, 83, 97};
        Arrays.sort(array, new ArrayComparator());
        Assert.assertArrayEquals(resultArray, array);
    }

    @Test(expected = NumberFormatException.class)
    public void testArraySortingNegative() {
        Integer[] array = {Integer.valueOf("sdg"), 16, 8, 4, 83, 97, 82};
        Integer[] resultArray = {4, 6, 16, 8, 82, 83, 97};
        Arrays.sort(array, new ArrayComparator());
        Assert.assertArrayEquals(resultArray, array);
    }
}
