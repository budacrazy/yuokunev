package HW3_2;

import java.util.Arrays;

public class Median {
    public Median() {
    }

    public static float median(int[] array) {
        Arrays.sort(array);
        return array.length % 2 != 0 ? (float)array[array.length / 2] : (float)((double)(array[(array.length - 1) / 2] + array[array.length / 2]) / 2.0D);
    }

    public static double median(double[] array) {
        Arrays.sort(array);
        return array.length % 2 != 0 ? array[array.length / 2] : (array[(array.length - 1) / 2] + array[array.length / 2]) / 2.0D;
    }
}
