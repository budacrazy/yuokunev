package HW3_1;

import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Test;

public class CardTest {
    public CardTest() {
    }

    @Test
    public void withdrawBalanceTest() {
        double balance = 2000.58D;
        double withdrawValue = 550.38D;
        double result = 1450.2D;
        Card card = new Card("Tester", balance);
        card.withdrawBalance(withdrawValue);
        Assert.assertEquals((long)BigDecimal.valueOf(result).compareTo(card.getBalance()), 0L);
    }

    @Test
    public void withdrawBalanceTestNegative() {
        double balance = 1000.0D;
        double withdrawValue = -500.0D;
        double result = 1000.0D;
        Card card = new Card("Tester", balance);
        card.withdrawBalance(withdrawValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void addBalanceFirstTest() {
        double balance = 2000.58D;
        double addValue = 550.385D;
        double result = 2550.965D;
        Card card = new Card("Tester", balance);
        card.addBalance(addValue);
        Assert.assertEquals((long)BigDecimal.valueOf(result).compareTo(card.getBalance()), 0L);
    }

    @Test
    public void addBalanceSecondTest() {
        double addValue = 550.0D;
        double result = 550.0D;
        Card card = new Card("Tester");
        card.addBalance(addValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void addBalanceTestNegative() {
        double balance = 2000.0D;
        double addValue = -550.0D;
        double result = 2000.0D;
        Card card = new Card("Tester", balance);
        card.addBalance(addValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void showBalanceInCurrencyTest() {
        Card card = new Card("Tester", 500.0D);
        double rate = 2.5D;
        double result = 1250.0D;
        Assert.assertEquals((long)BigDecimal.valueOf(result).compareTo(card.showBalanceInCurrency(rate)), 0L);
    }

    @Test
    public void showBalanceInCurrencyTestNegative() {
        Card card = new Card("Tester", 500.0D);
        double rate = -2.5D;
        double result = 1250.0D;
        Assert.assertEquals((long)BigDecimal.valueOf(result).compareTo(card.showBalanceInCurrency(rate)), 0L);
    }
}
