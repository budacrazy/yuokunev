package HW3_2;

import org.junit.Assert;
import org.junit.Test;

public class MedianTest {
    public MedianTest() {
    }

    @Test
    public void testIntMedian() {
        float result = Median.median(new int[]{5, 5, 5, 100, 5, 5, 5, 5});
        Assert.assertEquals(5.0F, result, 0.0F);
    }

    @Test
    public void testIntMedianOddNumber() {
        float result = Median.median(new int[]{1, 5, 2, 8, 7});
        Assert.assertEquals(5.0F, result, 0.0F);
    }

    @Test
    public void testIntMedianEvenNumber() {
        float result = Median.median(new int[]{1, 6, 2, 8, 7, 2});
        Assert.assertEquals(4.0F, result, 0.0F);
    }

    @Test
    public void testIntMedianEvenAverage() {
        float result = Median.median(new int[]{1, 2, 3, 4});
        Assert.assertEquals(2.5D, (double)result, 0.0D);
    }

    @Test
    public void testDoubleMedian() {
        double result = Median.median(new double[]{1.0D, 0.5D, 0.5D, 0.5D, 0.5D, 0.55D, 0.5D, 0.5D});
        Assert.assertEquals(0.5D, result, 0.0D);
    }

    @Test
    public void testDoubleMedianOddNumber() {
        double result = Median.median(new double[]{0.5D, 0.2D, 0.4D, 0.3D, 0.1D});
        Assert.assertEquals(0.3D, result, 0.0D);
    }

    @Test
    public void testDoubleMedianEvenAverage() {
        double result = Median.median(new double[]{0.1D, 0.2D, 0.3D, 0.4D, 0.2D, 0.5D});
        Assert.assertEquals(0.25D, result, 0.0D);
    }
}
