package HW6;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class SearchAlgorithmTest {

    private final String[][] outputArray = {{"A:", "a", "1"},
                                            {"  ", "and", "1"},
                                            {"B:", "boy", "1"},
                                            {"G:", "good", "1"},
                                            {"H:", "he", "1"},
                                            {"  ", "hello", "1"},
                                            {"I:", "in", "1"},
                                            {"  ", "is", "2"},
                                            {"L:", "live", "1"},
                                            {"M:", "mogilev", "1"},
                                            {"  ", "my", "1"},
                                            {"N:", "name","1"},
                                            {"Y:", "yuri", "2"}};

    private final String inputStr = "Hello my name is Yuri. Yuri is a good boy and he live in Mogilev";

    @Test
    public void AlgorithmTest(){
        SearchAlgorithm search = new SearchAlgorithm();
        assertArrayEquals(outputArray, search.getFinalSortedArray(inputStr));
    }
}
