package HW6;

import java.sql.Array;
import java.util.*;

public class SearchAlgorithm {

        public String getInputString(){
            Scanner scanner = new Scanner(System.in);
            return scanner.nextLine();
        }

        public String[][] getFinalSortedArray(String inputStr){
            String[] allWords = inputStr.toLowerCase(Locale.ROOT).split("\\W+");
            Arrays.sort(allWords);
            int countNoRepeatWords = 0;
            List<ArrayList<String>> finalListOfWords = new ArrayList<>();
            for (int indexWord = 0; indexWord < allWords.length; indexWord++) {
               int countRepeatWords = 0;
               finalListOfWords.add(new ArrayList<>());
                for (int index = indexWord; index < allWords.length; index++) {
                    if (allWords[indexWord].equals(allWords[index])) {
                        countRepeatWords++;
                    } if(!allWords[indexWord].equals(allWords[index]) || allWords[indexWord].equals(allWords[index]) && index == allWords.length - 1){
                        finalListOfWords.get(countNoRepeatWords).add(0, String.valueOf(allWords[indexWord].charAt(0)).toUpperCase(Locale.ROOT) + ":");
                        finalListOfWords.get(countNoRepeatWords).add(1, allWords[indexWord]);
                        finalListOfWords.get(countNoRepeatWords).add(2, String.valueOf(countRepeatWords));
                        countNoRepeatWords++;
                        break;
                    }
                }
                indexWord = indexWord + countRepeatWords - 1;
            }
            return correctionGrouping(convertListToArray(finalListOfWords));
        }
    public String[][] convertListToArray(List<ArrayList<String>> list){
            String[][] array = new String[list.size()][list.get(0).size()];
            for(int i = 0; i < list.size(); i++){
                for (int j = 0; j < list.get(0).size(); j++){
                    array[i][j] = list.get(i).get(j);
                }
            }
            return array;
        }
    public String[][] correctionGrouping(String[][] array){
        for(int i = 0; i < array.length-1; i++){
            if(Objects.equals(array[i][0], array[i+1][0])) { array[i+1][0] = "  "; }
        }
        return array;
    }
        public void printArray(String[][] array){
            for (int i = 0; i < array.length; i++){
                for (int j = 0; j < array[0].length; j++){
                    System.out.print(array[i][j]);
                }
                System.out.println();
            }
        }
}
