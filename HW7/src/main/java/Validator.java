public interface Validator <T> {
    void validate(T variable) throws ValidationFailedException;
}
