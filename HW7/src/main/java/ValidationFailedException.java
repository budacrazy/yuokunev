public class ValidationFailedException extends Exception {
    ValidationFailedException(String message){
        super(message);
    }
}
