import java.util.HashMap;
import java.util.Map;

public class ValidationSystem {

   public static <T> void validate(T variable) throws ValidationFailedException {
      Map<Object, Validator> validators = new HashMap<>();
      validators.put(String.class, new StringValidator());
      validators.put(Integer.class,new IntegerValidator());
      validators.get(variable.getClass()).validate(variable);
   }


}
