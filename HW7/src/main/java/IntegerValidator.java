public class IntegerValidator implements Validator<Integer>{
    @Override
    public void validate(Integer variable) throws ValidationFailedException {
        if(variable < 1 || variable > 10){
            throw new ValidationFailedException("IntegerFailedValidation");
        }
    }
}
