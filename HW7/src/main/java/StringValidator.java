public class StringValidator implements Validator<String>{
    @Override
    public void validate(String variable) throws ValidationFailedException {
        if(!variable.matches("[A-ZЁА-Я].+")) {
            throw new ValidationFailedException("StringFailedValidation");
        }
    }

}
