package HW4_1;

public class DebitCard extends Card{

    public DebitCard(String holderName, double accountBalance){
        super(holderName, accountBalance);
    }

    public DebitCard(String holderName) { super(holderName); }

    @Override
    public void withdrawBalance(double amount) {
        super.withdrawBalance(amount);
    }
}
