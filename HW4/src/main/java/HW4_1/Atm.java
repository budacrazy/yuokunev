package HW4_1;

public class Atm {

    public void addBalance(Card cardTo, double amount) {
        cardTo.addBalance(amount);
    }

    public void withdrawBalance(Card cardTo, double amount) {
        cardTo.withdrawBalance(amount);
    }
}
