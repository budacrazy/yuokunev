package HW4_1;

import java.math.BigDecimal;

public class CreditCard extends Card{

    public CreditCard(String holderName, double accountBalance) {
        super(holderName, accountBalance);
    }

    public CreditCard(String holderName){ super(holderName);}

    @Override
    public void withdrawBalance(double amount) {
        if (amount < 0) {
            System.out.println("Amount not valid");
        } else {
            super.accountBalance = super.accountBalance.subtract(BigDecimal.valueOf(amount));
        }
    }
}
