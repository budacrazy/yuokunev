package HW4_1;

import java.math.BigDecimal;

public class Card {
    protected String holderName;
    protected BigDecimal accountBalance;

    public String getHolderName() {
        return this.holderName;
    }

    public BigDecimal getBalance() {
        return this.accountBalance;
    }

    public Card(String holderName, double accountBalance) {
        this.holderName = holderName;
        this.accountBalance = BigDecimal.valueOf(accountBalance);
    }

    public Card(String holderName) {
        this(holderName, 0);
    }

    public void addBalance(double amount) {
        if (amount < 0) {
            System.out.println("Amount not valid");
        } else {
            this.accountBalance = this.accountBalance.add(BigDecimal.valueOf(amount));
        }

    }

    public void withdrawBalance(double amount) {
        if (amount < 0) {
            System.out.println("Amount not valid");
        } else if (this.accountBalance.compareTo(BigDecimal.valueOf(amount)) < 0) {
            System.out.println("You dont have enough money");
        } else {
            this.accountBalance = this.accountBalance.subtract(BigDecimal.valueOf(amount));
        }

    }

    public BigDecimal showBalanceInCurrency(double rate) {
        return this.accountBalance.multiply(BigDecimal.valueOf(Math.abs(rate)));
    }
}
