package HW4_2;

public class BubbleSort implements Sorter {

    @Override
    public int[] sort(int[] array) {
        boolean unSorted = true;
        int temp;
        while (unSorted) {
            unSorted = false;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    unSorted = true;
                }
            }
        }
        return array;
    }
}
