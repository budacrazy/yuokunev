package HW4_2;

public class SortingContext {

    private Sorter sorter;

    public SortingContext(Sorter sorter) { this.sorter = sorter; }

    public int[] executeSort(int[] array) {
        sorter.sort(array);
        return array;
    }
}
