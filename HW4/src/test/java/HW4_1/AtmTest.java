package HW4_1;

import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Test;

public class AtmTest {

    Atm atm = new Atm();

    @Test
    public void withdrawBalanceDebitTest() {
        double balance = 2000;
        double withdrawValue = 550;
        double result = 1450;
        Card card = new DebitCard("Tester", balance);
        atm.withdrawBalance(card, withdrawValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void withdrawBalanceDebitTestNegative() {
        double balance = 1000;
        double withdrawValue = 1100;
        double result = 1000;
        Card card = new DebitCard("Tester", balance);
        atm.withdrawBalance(card, withdrawValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void addBalanceDebitTest() {
        double balance = 2000;
        double addValue = 550;
        double result = 2550;
        Card card = new DebitCard("Tester", balance);
        atm.addBalance(card, addValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void addBalanceDebitTestNegative() {
        double balance = 2000;
        double addValue = -550;
        double result = 2000;
        Card card = new DebitCard("Tester", balance);
        atm.addBalance(card, addValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void withdrawBalanceCreditTest() {
        double balance = 1000;
        double withdrawValue = 1100;
        double result = -100;
        Card card = new CreditCard("Tester", balance);
        atm.withdrawBalance(card, withdrawValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void withdrawBalanceCreditTestNegative() {
        double balance = 1000;
        double withdrawValue = -1100;
        double result = 1000;
        Card card = new CreditCard("Tester", balance);
        atm.withdrawBalance(card, withdrawValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void addBalanceCreditTest() {
        double balance = 2000;
        double addValue = 550;
        double result = 2550;
        Card card = new CreditCard("Tester", balance);
        atm.addBalance(card, addValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }

    @Test
    public void addBalanceCreditTestNegative() {
        double balance = 2000;
        double addValue = -550;
        double result = 2000;
        Card card = new CreditCard("Tester", balance);
        atm.addBalance(card, addValue);
        Assert.assertEquals(BigDecimal.valueOf(result), card.getBalance());
    }
}
