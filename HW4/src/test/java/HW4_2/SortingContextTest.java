package HW4_2;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class SortingContextTest {

    private final int[] arrayInitial = {5, -6, 2, -3, 8, -15};

    private final int[] arraySorted = {-15, -6, -3, 2, 5, 8};

    @Test
    public void BubbleStrategyTest(){
        Sorter bubbleSorting = new BubbleSort();
        SortingContext context = new SortingContext(bubbleSorting);
        assertArrayEquals(arraySorted, context.executeSort(arrayInitial));
    }
    @Test
    public void SelectionStrategyTest(){
        Sorter selectionSorting = new SelectionSort();
        SortingContext context = new SortingContext(selectionSorting);
        assertArrayEquals(arraySorted, context.executeSort(arrayInitial));
    }
}
